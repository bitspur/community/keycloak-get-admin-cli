/**
 * File: /package.json
 * Project: keycloak-get-admin-cli
 * File Created: 29-08-2021 15:04:10
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 30-08-2021 15:03:23
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare global {
  namespace Cypress {
    interface Chainable<Subject = any> {
      login: typeof login;
    }
  }
}

export function login(
  username: string,
  password: string,
  { cacheSession }: Partial<LoginOptions>
) {
  const login = () => {
    cy.visit('/auth/admin/master/console');
    cy.wait(1000);
    cy.get('#username').click();
    cy.get('#username').type(username);
    cy.get('#password').click();
    cy.get('#password').type(password);
    cy.get('#kc-login').click();
  };
  if (cacheSession) {
    cy.session([username, password], login);
  } else {
    login();
  }
}

export interface LoginOptions {
  cacheSession: boolean;
}

Cypress.Commands.add('login', login);
