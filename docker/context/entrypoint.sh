#!/bin/sh

KEYCLOAK_GET_ADMIN_CLI="node /opt/app/bin/keycloakGetAdminCli"

cat <<EOF > .env
HEADLESS=true
KEYCLOAK_ADMIN_PASSWORD=******
KEYCLOAK_ADMIN_USERNAME=$KEYCLOAK_ADMIN_USERNAME
KEYCLOAK_BASE_URL=$KEYCLOAK_BASE_URL
EOF
cat .env 1>&2

until $(curl -s --max-time 10 --fail ${KEYCLOAK_BASE_URL}/auth > /dev/null); do
  printf '.' 1>&2
  sleep 5
done

ADMIN_CLI_CLIENT_CREDENTIAL=$($KEYCLOAK_GET_ADMIN_CLI | grep "ADMIN_CLI_CLIENT_CREDENTIAL" | sed 's|^ADMIN_CLI_CLIENT_CREDENTIAL: ||g')

if [ "$ADMIN_CLI_CLIENT_CREDENTIAL" = "" ]; then
  echo "failed to get admin cli client credential" 1>&2
  exit 1
fi

if [ "$SECRET_NAME" = "" ]; then
  echo $ADMIN_CLI_CLIENT_CREDENTIAL
  exit 0
fi

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: $SECRET_NAME
  $([ "$SECRET_NAMESPACE" = "" ] && echo || echo "namespace: $SECRET_NAMESPACE")
stringData:
  ADMIN_CLI_CLIENT_CREDENTIAL: |-
    $ADMIN_CLI_CLIENT_CREDENTIAL
EOF
