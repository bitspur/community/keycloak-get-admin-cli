/**
 * File: /package.json
 * Project: keycloak-get-admin-cli
 * File Created: 29-08-2021 11:13:51
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 30-08-2021 15:03:23
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// <reference types="cypress" />
/// <reference types="cypress-promise" />
/// <reference types="cypress-wait-until" />
/// <reference types="cypress-xpath" />

import dotenv from 'dotenv';
dotenv.config();

describe(
  'keycloak credentials',
  { baseUrl: Cypress.env('KEYCLOAK_BASE_URL') },
  () => {
    before(() => {
      if (!Cypress.env('username')) {
        Cypress.env('username', Cypress.env('KEYCLOAK_ADMIN_USERNAME'));
      }
      if (!Cypress.env('password')) {
        Cypress.env('password', Cypress.env('KEYCLOAK_ADMIN_PASSWORD'));
      }
    });

    beforeEach(() => {
      cy.login(Cypress.env('username') || '', Cypress.env('password') || '', {
        cacheSession: true
      });
      cy.visit('/auth/admin/master/console/#/realms/master/clients');
    });

    it('will get admin cli client secret', () => {
      cy.get('table').contains('admin-cli').click();
      cy.get('div[data-ng-controller="ClientTabCtrl"]')
        .contains('Admin-cli')
        .should('be.visible');
      cy.get('#accessType').then(($elem: JQuery<any>) => {
        const accessType = ($elem.val() || '').toString();
        if (accessType !== 'string:confidential') {
          cy.log("setting access type to 'confidential'");
          cy.get('.border-top > .form-group:nth-child(13)').click();
          cy.get('#accessType').select('confidential');
          cy.get('button[type="submit"]').contains('Save').click();
          cy.wait(1000);
        } else {
          cy.log("access type already 'confidential'");
        }
        cy.get('.nav-tabs > li:nth-child(2) > .ng-binding').click();
        cy.waitUntil(() =>
          cy
            .get('form[data-ng-controller="ClientSecretCtrl"] #secret')
            .then(($elem: JQuery) => ($elem.val() || '').toString().length > 0)
        );
        cy.get('form[data-ng-controller="ClientSecretCtrl"] #secret').then(
          ($elem: JQuery) => {
            const adminCliClientCredential = ($elem.val() || '').toString();
            const tmpPath = Cypress.env('tmpPath');
            cy.log(`ADMIN_CLI_CLIENT_CREDENTIAL: ${adminCliClientCredential}`);
            if (tmpPath) {
              cy.writeFile(tmpPath, adminCliClientCredential);
            }
          }
        );
      });
    });
  }
);
