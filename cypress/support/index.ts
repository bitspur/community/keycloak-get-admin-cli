/**
 * File: /package.json
 * Project: keycloak-get-admin-cli
 * File Created: 29-08-2021 11:13:08
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 30-08-2021 15:03:23
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/// <reference types="cypress" />

import 'cypress-promise/register';
import 'cypress-wait-until';
import 'cypress-xpath';
import './commands';
