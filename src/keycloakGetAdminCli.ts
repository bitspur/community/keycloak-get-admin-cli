/**
 * File: /src/keycloakGetAdminCli.ts
 * Project: keycloak-get-admin-cli
 * File Created: 29-08-2021 15:29:20
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 30-08-2021 15:06:22
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import cypress from 'cypress';
import fs from 'fs-extra';
import os from 'os';
import path from 'path';

export default class KeycloakGetAdminCli {
  constructor(
    private readonly baseUrl: string,
    private readonly headless: boolean = true
  ) {}

  async getClientSecret(username: string, password: string): Promise<string> {
    const tmpPathDir = path.resolve(os.tmpdir(), 'keycloakCredentials');
    const tmpPath = path.resolve(tmpPathDir, Date.now().toString());
    await fs.mkdirs(tmpPathDir);
    await cypress.run({
      spec: './cypress/integration/keycloakGetAdminCliClientSecret.spec.ts',
      headed: !this.headless,
      browser: 'electron',
      config: {
        baseUrl: this.baseUrl,
        experimentalSessionSupport: true,
        video: false
      },
      env: {
        password,
        tmpPath,
        username
      }
    });
    const result = (await fs.readFile(tmpPath)).toString();
    await fs.remove(tmpPath);
    return result;
  }
}
